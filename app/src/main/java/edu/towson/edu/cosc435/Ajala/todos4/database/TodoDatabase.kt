package edu.towson.edu.cosc435.Ajala.todos4.database

import androidx.lifecycle.LiveData
import androidx.room.*
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import java.util.*

@Dao
interface TodoDao {
    @Insert
    suspend fun addTodo(todo: Todo)

    @Update
   suspend fun updateTodo(todo: Todo)

    @Delete
    suspend fun deleteTodo(todo: Todo)

    @Query("SELECT * FROM Todo where title = :title ")
    suspend fun getATodo(title: String):Todo?

    @Query("select id, title, content, completed, icon_url, date from Todo ")
   suspend fun getAllTodo():List<Todo>

}
class UUIDConverter{
    @TypeConverter
    fun fromString(uuid: String):UUID{
        return UUID.fromString(uuid)
    }
    @TypeConverter
    fun toString(uuid: UUID): String{
        return uuid.toString()
    }
}

@Database(entities = arrayOf(Todo::class), version = 2, exportSchema = false)
@TypeConverters(UUIDConverter::class)
abstract class TodoDatabase  : RoomDatabase(){
    abstract fun todoDao(): TodoDao
}