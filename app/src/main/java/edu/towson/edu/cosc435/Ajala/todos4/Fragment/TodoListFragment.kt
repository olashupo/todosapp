package edu.towson.edu.cosc435.Ajala.todos4.Fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import edu.towson.edu.cosc435.Ajala.todos4.Interface.TodoController
import edu.towson.edu.cosc435.Ajala.todos4.R
import edu.towson.edu.cosc435.Ajala.todos4.TodoAdapter
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class TodoListFragment : Fragment() {

    private lateinit var todoController: TodoController

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(context){
            is TodoController -> todoController = context
            else -> throw Exception("TodoController expected")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = TodoAdapter(todoController)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        addTodoBTN?.setOnClickListener { todoController.launchAdd() }



        val deletedTod:String? = null
        val touchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN or  ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT,1){
            override fun onMove(

                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                todoController.launch {  val sourcePosition :Int = viewHolder.layoutPosition
                    val targetPosition = target.absoluteAdapterPosition
                    suspend { Collections.swap(todoController.todostemp.getAll(),sourcePosition,targetPosition) }
                    recyclerView.adapter?.notifyItemMoved(sourcePosition,targetPosition)
                    adapter.notifyItemMoved(sourcePosition,targetPosition)
                }
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val postion =  viewHolder.absoluteAdapterPosition
                when(direction){
                    ItemTouchHelper.LEFT ->{
                        deletedTod?: todoController.todostemp.getTodo(postion)
                        suspend { todoController.todostemp.remove(todoController.todostemp.getTodo(postion)) }
                        recyclerView.adapter?.notifyItemRemoved(postion)
                        if (deletedTod != null) {
                            Snackbar.make(recyclerView, deletedTod, Snackbar.LENGTH_LONG)
                        }
                    }
                }
            }

        })
        touchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onResume() {
        super.onResume()
        todoController.launch{
            todoController.todostemp.getAll()
            recyclerView?.adapter?.notifyDataSetChanged()
        }
    }
}
