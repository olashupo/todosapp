package edu.towson.edu.cosc435.Ajala.todos4.Fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import edu.towson.edu.cosc435.Ajala.todos4.Interface.TodoController
import edu.towson.edu.cosc435.Ajala.todos4.R
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import kotlinx.android.synthetic.main.fragment_add_todo.*
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class AddTodoFragment : Fragment() {

    private lateinit var todoController: TodoController

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when(context){
            is TodoController -> todoController = context
            else -> throw Exception("TodoController expected")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_todo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        save_btn.setOnClickListener{handleAddTodoClick()}

            val todo = todoController.getTodoForEdit()
            populateTodoForm(todo)

    }
    private  fun populateTodoForm(todo: Todo?){
        todoController.launch {
            if(todo != null) {
                clearForm()
                title_txt.editableText.insert(0,todo.title)
                content_txt.editableText.insert(0,todo.content)
                iscompleted_check.isChecked = todo.IsCompleted
                val today = Calendar.getInstance()
               // val date = SimpleDateFormat("EEE MMMM d H:m:s Y").format(today.time)
                add_edit_title?.text ="Edit Todo"
                save_btn.text = "Edit Todo"
            }else{
                add_edit_title?.text ="Add Todo"
                save_btn.text = "Add Todo"
            }
        }
    }

    private fun clearForm(){
        todoController.launch {
            val today = Calendar.getInstance()
            val date = SimpleDateFormat("EEE MMMM d H:m:s Y").format(today.time)
            title_txt.editableText.clear()
            content_txt.editableText.clear()
            iscompleted_check.isChecked = false
        }

    }

    private fun handleAddTodoClick() {
                    val id =getTodoId()
                    val today = Calendar.getInstance()
                    val date = SimpleDateFormat("EEE MMMM d H:m:s Y").format(today.time)
                    val title = title_txt.text.toString()
                    val text = content_txt.text.toString()
                    val complete = iscompleted_check.isChecked
                    val iconUrl = ""
                    val todo = Todo(id, title, text, complete, iconUrl,date)

        todoController.launch {
            try {
                if (todoController.getTodoForEdit() == null) {
                    todoController.addNewTodo(todo)
                } else {
                    todoController.handleEditedTodo(todo)
                    save_btn.text = "Add Todo"
                }
                clearForm()
            } catch (e: Exception) {
            }
        }
    }

    fun populateTodo(){
        val todo = todoController.getTodoForEdit()
        populateTodoForm(todo)
    }

    private fun getTodoId(): UUID{
                val id:UUID
                val editTodo = todoController.getTodoForEdit()
                if (editTodo == null){
                    //do not edit rather generate a new id
                    id = UUID.randomUUID()
                }else{
                    id=editTodo.id
                }

        return id
    }
}
