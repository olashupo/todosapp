package edu.towson.edu.cosc435.Ajala.todos4

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.towson.edu.cosc435.Ajala.todos4.Interface.TodoController
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import kotlinx.android.synthetic.main.todolist.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class TodoAdapter (private val controller:TodoController): RecyclerView.Adapter<CustomViewHolder>(){

    override fun getItemCount(): Int {
        return controller.getCurrentCount()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context).inflate(R.layout.todolist, parent, false)
        val viewHolder = CustomViewHolder(layoutInflater)

        layoutInflater.completeview.setOnClickListener {
            val position = viewHolder.adapterPosition
            controller.launch {
                try {
                    val todo = controller.todostemp.getTodo(position)
                    controller.complete(todo)
                    this@TodoAdapter.notifyItemChanged(position)
                }catch (e: Exception){
                    throw e
                }
            }


        }

        layoutInflater.setOnClickListener{
            val position = viewHolder.adapterPosition
            controller.launch{
                try {
                    controller.EditTodo(position)
                }catch (e: Exception){

                }
            }
        }

        layoutInflater.setOnLongClickListener {
            val position = viewHolder.adapterPosition
            val dialogBuilder = AlertDialog.Builder(layoutInflater.context)
            val todo = controller.todostemp.getTodo(position)
            dialogBuilder.setMessage("Would you like to delete?")
                .setCancelable(false)
                .setPositiveButton("Proceed", DialogInterface.OnClickListener {
                        dialog, id ->  controller.launch{
                    try {
                        controller.deleteTodo(todo)
                        this@TodoAdapter.notifyItemRemoved(position)
                    }catch (e:Exception){
                    }
                }
                })
                .setNegativeButton("Cancel", DialogInterface.OnClickListener {
                        dialog, id -> dialog.cancel()
                })
            val alert = dialogBuilder.create()
            alert.setTitle(controller.todostemp.getTodo(position).title)
            alert.show()
            return@setOnLongClickListener true
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val todo = controller.todostemp.getTodo(position)
        holder.BindTodo(controller,todo)
    }
}

class CustomViewHolder(view: View): RecyclerView.ViewHolder(view) {
    fun BindTodo(controller: TodoController,todo:Todo?){
        if (todo!!.IsCompleted) {
            itemView.titleview_txt.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            itemView.todoview_txt.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        }
        else {
            itemView.titleview_txt.paintFlags = 0
            itemView.todoview_txt.paintFlags = 0
        }
        if (todo.content.length > 200){
            itemView.todoview_txt.text = todo.content.subSequence(0,199)
        }
        else{
            itemView.todoview_txt.text = todo.content
        }

        if (todo.title.length > 200){
            itemView.titleview_txt.text = todo.title.substring(0, 199)
        }
        else{
            itemView.titleview_txt.text = todo.title
        }
        val today = Calendar.getInstance()
        itemView.dateview_txt.text = SimpleDateFormat("EEE MMMM d H:m:s Y").format(today.time)
        itemView.completeview.isChecked = todo.IsCompleted
        itemView.iconProgress.visibility = View.VISIBLE
        itemView.todoIconImage.visibility = View.INVISIBLE
        controller.launch(Dispatchers.Main) {
           val bitmap = controller.fetchIcon(todo.iconUrl)
            itemView.todoIconImage.setImageBitmap(bitmap)
            itemView.iconProgress.visibility = View.INVISIBLE
            itemView.todoIconImage.visibility = View.VISIBLE
        }
    }

}