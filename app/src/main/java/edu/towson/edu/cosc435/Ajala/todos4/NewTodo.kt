package edu.towson.edu.cosc435.Ajala.todos4
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import kotlinx.android.synthetic.main.activity_new_todo.*
import java.text.SimpleDateFormat
import java.util.*

class NewTodo : AppCompatActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        save_btn.setOnClickListener{handleAddTodoClick()}

    }
    private fun handleAddTodoClick() {
        when (null) {
            R.id.save_btn -> {
                val titletest = title_txt.text
                val contenttest = content_txt.text
                if (titletest.isEmpty() || contenttest.isEmpty()) {
                    Toast.makeText(this, "The Title or Content is missing", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val intent = Intent()
                    val id = UUID.randomUUID()
                    val today = Calendar.getInstance()
                    val date = SimpleDateFormat("EEE MMMM d H:m:s Y").format(today.time)
                    val title = title_txt.text.toString()
                    val text = content_txt.text.toString()
                    val complete = iscompleted_check.isChecked
                    val iconUrl =""

                    val todo = Todo(id, title, text, complete, iconUrl,date)
                    val json: String = Gson().toJson(todo)
                    intent.putExtra(TODO_EXTRA_KEY, json)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }

            }
        }
    }

    companion object{
        val TODO_EXTRA_KEY = "Todo"
    }

}

