package edu.towson.edu.cosc435.Ajala.todos4


import android.os.Build
import androidx.annotation.RequiresApi
import edu.towson.edu.cosc435.Ajala.todos4.Interface.ITodoRepository
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import okhttp3.internal.userAgent
import java.text.SimpleDateFormat
import java.util.*

class TodoRepository() : ITodoRepository{

    private var todos: MutableList<Todo> = mutableListOf()
    val today = Calendar.getInstance()
    val date = SimpleDateFormat("EEE MMMM d H:m:s Y").format(today.time)

    init {
      // val seed = (1..10).map { idx -> Todo(UUID.randomUUID(), "Title${idx}", "Content${idx}", true,  userAgent) }
      // todos.addAll(seed)
    }

    override fun getCount(): Int {
        return todos.size
    }

    override fun getTodo(idx: Int): Todo {
        return todos.get(idx)
    }

    override suspend fun getAll(): List<Todo> {
        return todos
    }

    override suspend fun remove(todo: Todo) {
        todos.remove(todo)
    }

    override suspend fun replace(idx: Int, todo: Todo) {
        if (idx >= todos.size) {
            throw Exception("Out of bounds!")
        }
        todos[idx] = todo
    }

    override fun IsCompleted(todo: Todo) {
        var check = !todo.IsCompleted
        todo.IsCompleted = check
        var position = 0
        val id = todo.id

        while (position < todos.size){
            if (todos[position].id == id){
                todos[position].IsCompleted = check
                break
            }
            position++
        }
    }

    override suspend fun addTodo(todo: Todo) {
        todos.add(todo)
    }


}