package edu.towson.edu.cosc435.Ajala.todos4
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import edu.towson.edu.cosc435.Ajala.todos4.Fragment.AddTodoFragment
import edu.towson.edu.cosc435.Ajala.todos4.Interface.ITodoRepository
import edu.towson.edu.cosc435.Ajala.todos4.Interface.TodoController
import edu.towson.edu.cosc435.Ajala.todos4.Network.ITodoApi
import edu.towson.edu.cosc435.Ajala.todos4.Network.TodoApi
import edu.towson.edu.cosc435.Ajala.todos4.database.TodoDatabase
import edu.towson.edu.cosc435.Ajala.todos4.database.TodoDatabaseRepository
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.lang.Exception
import kotlin.coroutines.CoroutineContext


class MainActivity() : AppCompatActivity(), TodoController{

    override lateinit var todostemp:ITodoRepository
    private var editingTodo: Todo? = null
    private var editingTodoIdx: Int = -1
    private lateinit var todoApi: ITodoApi

    private fun showSpinner(){
        progressBar.visibility = View.VISIBLE
    }

    private fun hideSpinner(){
        progressBar.visibility = View.GONE
    }


    override suspend fun addNewTodo(todo: Todo) {
        showSpinner()
        try {
                withContext(Dispatchers.IO) {
                    todostemp.addTodo(todo)
                }

            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                findNavController(R.id.nav_host_fragment)
                    .popBackStack()
            }else{
                recyclerView.adapter?.notifyDataSetChanged()
            }
        }catch (e: Exception){
            Log.e(TAG, "Error: $(e.message)")
            Toast.makeText(this, "Failed to add new Todo", Toast.LENGTH_SHORT).show()
            throw e
        }finally {
            hideSpinner()
        }


    }

    override fun EditTodo(idx: Int) {
       val todo = todostemp.getTodo(idx)
        editingTodo = todo
        editingTodoIdx = idx
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            findNavController(R.id.nav_host_fragment)
                .navigate(R.id.action_todoListFragment_to_addTodoFragment2)
        }
        else{
           // landscape
            (addTodoFragment as AddTodoFragment).populateTodo()
        }

    }

    override fun getTodoForEdit(): Todo? {
        return editingTodo
    }

    override suspend fun handleEditedTodo(todo: Todo) {
        showSpinner()
        try {
            todostemp.replace(editingTodoIdx, todo)
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                findNavController(R.id.nav_host_fragment)
                    .popBackStack()
            }else{
                recyclerView.adapter?.notifyItemChanged(editingTodoIdx)
            }
            clearEditingTodo()
        }catch (e:Exception){
            Log.e(TAG, "Error: $(e.message)")
            Toast.makeText(this, "Failed to update Todo", Toast.LENGTH_SHORT).show()
            throw e
        }finally {
            hideSpinner()
        }
    }

    override fun clearEditingTodo() {
        editingTodo = null
        editingTodoIdx = -1
    }

    override suspend fun fetchTodos(): List<Todo> {
        return todoApi.fetchTodos().await()
    }


    override suspend fun fetchIcon(url: String): Bitmap {
        return todoApi.fetchIcon(url).await()
    }

    override suspend fun checkCache(icon: String): Bitmap? {
        val file = File(cacheDir, icon)
        if (file.exists()){
            val input = file.inputStream()
            return BitmapFactory.decodeStream(input)
        }else{
            return null
        }
    }

    override suspend fun cacheIcon(filename: String, icon: Bitmap) {
        val file = File(cacheDir, filename)
        val output = file.outputStream()
        icon.compress(Bitmap.CompressFormat.JPEG, 100, output)
    }

    override val coroutineContext: CoroutineContext
        get() = lifecycleScope.coroutineContext

    override fun launchAdd() {
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            findNavController(R.id.nav_host_fragment)
                .navigate(R.id.action_todoListFragment_to_addTodoFragment2)
        }
    }

    override fun getCurrentCount() : Int {
        return todostemp.getCount()
    }

    override suspend fun deleteTodo(todo: Todo) {
        showSpinner()
        try {
            withContext(Dispatchers.IO){
                todostemp.remove(todo)
            }
        }catch (e: Exception){
            Log.e(TAG, "Error: $(e.message)")
            throw e
        }finally {
            hideSpinner()
        }
    }

    override fun complete(todo: Todo) {
        todostemp.IsCompleted(todo)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        todoApi = TodoApi(this)

        todostemp = TodoDatabaseRepository(this)
        launch {
            val todoListAPI = fetchTodos()
            val todoListDB = todostemp.getAll()
            todoListAPI.forEach { todoAPI ->
                if (todoListDB.firstOrNull{todoDB -> todoDB.id == todoAPI.id} == null){
                    todostemp.addTodo(todoAPI)
                }
            }
        }
        val scheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val jobInfo = JobInfo.Builder(JOB_ID, ComponentName(this, TodosServices::class.java))
        jobInfo.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
        jobInfo.setMinimumLatency(15*1000)
        scheduler.schedule(jobInfo.build())

        MessageQueue.Channel.observe(this, Observer<Todo> { todo ->
            Log.d(TAG, "Received new todo from TodosServices: $todo")
            NotificationManagerCompat.from(this@MainActivity).cancel(TodosServices.NOTID_ID)
            launch(Dispatchers.IO){
                (todostemp as TodoDatabaseRepository).refreshingTodoList()
                withContext(Dispatchers.Main) {
                    recyclerView.adapter?.notifyDataSetChanged()
                }
            }
        })
    }

    override fun onStop() {
        super.onStop()
        this.cancel()
    }
    companion object{
        val TAG = MainActivity::class.java.simpleName
        val JOB_ID = 1
    }
}
