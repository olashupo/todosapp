package edu.towson.edu.cosc435.Ajala.todos4.database

import android.content.Context
import androidx.room.Room
import edu.towson.edu.cosc435.Ajala.todos4.Interface.ITodoRepository
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import kotlinx.coroutines.delay

class TodoDatabaseRepository(ctx: Context) :ITodoRepository{
    private val todoList: MutableList<Todo> = mutableListOf()
    private val db:TodoDatabase

        init {
            db = Room.databaseBuilder(ctx,
            TodoDatabase::class.java,
            "todo.db").build()
        }

    override fun getCount(): Int {
        return todoList.size
    }

    override suspend fun replace(idx: Int, todo: Todo) {
        delay(2000)
        // this will throw an exception randomly
        if (System.currentTimeMillis() % 2 == 0L) throw Exception()
        db.todoDao().updateTodo(todo)
        refreshingTodoList()
    }

    override fun IsCompleted(todo: Todo) {
        val check = !todo.IsCompleted
        todo.IsCompleted = check
        var position = 0
        val id = todo.id

        while (position < todoList.size){
            if (todoList[position].id == id){
                todoList[position].IsCompleted = check
                break
            }
            position++
        }
    }

    override suspend fun remove(todo: Todo) {
        delay(2000)
        // this will throw an exception randomly
        if (System.currentTimeMillis() % 2 == 0L) throw Exception()
       db.todoDao().deleteTodo(todo)
        refreshingTodoList()
    }

    override fun getTodo(idx: Int): Todo {
        return todoList.get(idx)
    }

    override suspend fun getAll(): List<Todo> {
        refreshingTodoList()
        return todoList
    }

    override suspend fun addTodo(todo: Todo) {
        db.todoDao().addTodo(todo)
        refreshingTodoList()
    }
     suspend fun refreshingTodoList(){
        todoList.clear()
        val todo = db.todoDao().getAllTodo()
        todoList.addAll(todo)
    }

}