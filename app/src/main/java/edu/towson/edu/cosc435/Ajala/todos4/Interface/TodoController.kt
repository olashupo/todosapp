package edu.towson.edu.cosc435.Ajala.todos4.Interface

import android.graphics.Bitmap
import androidx.recyclerview.widget.ItemTouchHelper
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import kotlinx.coroutines.CoroutineScope

interface TodoController: CoroutineScope{
    suspend fun deleteTodo(todo: Todo)
    fun complete(todo: Todo)
    fun launchAdd()
    fun getCurrentCount() : Int
    val todostemp: ITodoRepository
    suspend fun addNewTodo(todo:Todo)
    fun EditTodo(idx: Int)
    fun getTodoForEdit(): Todo?
    suspend fun handleEditedTodo(todo: Todo)
    fun clearEditingTodo()
    suspend fun fetchTodos(): List<Todo>
    suspend fun fetchIcon(url: String): Bitmap
    suspend fun checkCache(icon: String): Bitmap?
    suspend fun cacheIcon(filename: String, icon: Bitmap)

}