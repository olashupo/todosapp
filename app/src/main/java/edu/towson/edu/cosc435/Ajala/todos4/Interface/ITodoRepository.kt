package edu.towson.edu.cosc435.Ajala.todos4.Interface

import edu.towson.edu.cosc435.Ajala.todos4.models.Todo



interface ITodoRepository{
    fun getCount(): Int
  suspend  fun replace(idx: Int, todo: Todo)
    fun IsCompleted(todo: Todo)
  suspend  fun remove(todo: Todo)
    fun getTodo(idx: Int):Todo
   suspend fun getAll(): List<Todo>
   suspend fun addTodo(todo: Todo)



}