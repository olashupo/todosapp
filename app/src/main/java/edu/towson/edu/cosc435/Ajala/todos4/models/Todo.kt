package edu.towson.edu.cosc435.Ajala.todos4.models
import android.media.Image
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Todo(
    @PrimaryKey
    val id: UUID,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "content")
    val content: String,
    @ColumnInfo(name = "completed")
    @SerializedName("IsCompleted")
    var IsCompleted: Boolean,
    @ColumnInfo(name = "icon_url")
    @SerializedName("icon_url")
    val iconUrl: String,
    @ColumnInfo(name = "date")
    @SerializedName("date")
    val Date: String
   // val date: String
) {
    override fun toString():String{
        return "\n\n Title: $title\n\n Content: $content\n\n Complete $IsCompleted\n\n $iconUrl \n\n $Date"
    }
}