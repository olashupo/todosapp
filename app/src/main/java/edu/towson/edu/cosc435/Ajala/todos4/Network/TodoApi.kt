package edu.towson.edu.cosc435.Ajala.todos4.Network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import edu.towson.edu.cosc435.Ajala.todos4.Interface.TodoController
import edu.towson.edu.cosc435.Ajala.todos4.models.Todo
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.net.URL
import java.util.*
interface ITodoApi{
    suspend fun fetchTodos(): Deferred<List<Todo>>
    suspend fun fetchIcon(iconUrl: String): Deferred <Bitmap>
}

class TodoApi(val controller: TodoController): ITodoApi {

    private val BASE_URL: String ="https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"
    private val client: OkHttpClient = OkHttpClient()

    override suspend fun fetchTodos(): Deferred<List<Todo>> {
        return controller.async(Dispatchers.IO) {
            val request = Request.Builder()
                .url(BASE_URL)
                .get()
                .build()
            val result: String? = client.newCall(request).execute().body?.string()
            val todos: List<Todo> = parseJson(result)
             todos
        }
    }
    override suspend fun fetchIcon(iconUrl: String): Deferred<Bitmap> {
        delay(15*2000)
        return controller.async(Dispatchers.IO) {
            val filename = getImageFilename(iconUrl)
            val bitmap = controller.checkCache(filename)
            if(bitmap != null) {
                bitmap
            } else {
                val request = Request.Builder()
                    .url(iconUrl)
                    .get()
                    .build()
                val stream = client.newCall(request).execute().body?.byteStream()
                val bitmap = BitmapFactory.decodeStream(stream)
                if(bitmap != null)
                    controller.cacheIcon(filename, bitmap)
                bitmap
            }
        }
    }
    private  fun parseJson (json: String?): List<Todo>{
        val todos = mutableListOf<Todo>()
        if (json == null) return todos
        val jsonArr = JSONArray(json)
        var i =0
        while (i<jsonArr.length()){
            val jsonObj = jsonArr.getJSONObject(i)
            val todo = Todo(
                id = UUID.fromString(jsonObj.getString("id")),
                title = jsonObj.getString("title"),
                content = jsonObj.getString("contents"),
                IsCompleted = jsonObj.getBoolean("completed"),
                iconUrl = jsonObj.getString("image_url"),
                Date = jsonObj.getString("created")
            )
            todos.add(todo)
            i++
        }
        return todos
    }

    private fun getImageFilename(url: String): String {
        try{
             val urlObj = URL(url)
             val query = urlObj.query
             val filename = query.plus(".jpg")
            return filename

        return query.toString()

    }catch (e: Exception){
            return ""
        }
    }
}